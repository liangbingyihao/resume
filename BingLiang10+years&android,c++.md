# Bing Liang

## Senior Android Software Engineer

- Phone: +86 135-***-***
- Email: [bliang0623@163.com](bliang0623@163.com)

## Career Objective

With over 15 years of total development experience, which includes around 7 years of Android Application Development and Android Framework Development experience, I am seeking an Android Developer position where I can use my extensive android development and object oriented programming skills.


## Skills

* Skilled Android Software Developer with 6+ years of experience in architecting, building high performance enterprise scale mobile applications 
* Skilled C/Python/Java Developer with 10+ years of experience.
* Skilled Powerbuilder Developer
* Familiar with Object Oriented Programming.
* Familiar with REST Based Web Services.
* Familiar with Linux/Shell,Docker.
* Familiar with Mysql,Java,SpringBoot. 
* Familiar with srum development life cycle,Gitlab Flow,ci/cd.


## Education

[Beijing University of Posts and Telecommunications](https://en.wikipedia.org/wiki/Beijing_University_of_Posts_and_Telecommunications), Mathematics and Applied Mathematics, 2001 - 2005


## Experience

### 2018.6 - present [Huya Inc. 虎牙信息科技有限公司 NYSE: HUYA](https://www.huya.com/) 
### Senior Software Engineer, Engineering Productivity 
* (类似岗位：https://careers.google.com/jobs/results/72445869200155334-software-engineer-engineering-productivity/)
* (类似岗位：https://hr.huya.com/SocialRecruit/detail/5e1e9da1-f68b-4c47-95f8-ec6ccebf4d4f)

* **Project(所在项目,团队)** 
* Part of the software development team that involved in  test automation, refactoring code, test-driven development, build infrastructure, optimizing software, debugging, building tools and testing frameworks.
公司工程效率方向的软件开发团队，职责范围：自动化测试、重构代码，测试驱动开发等基层设施；代码调优、构建、测试框架。

* **Responsibilities(我的职责)** 
* Design and build automated test infrastructure for android application, using Android Espresso Framework,Android Ndk,Python etc... helping tester write ui testcase efficiently.
建设android端自动化测试基础设施，用到android espresso,ndk,Python等技术，帮助测试人员更高效编写ui测试用例
* Build distributed systems for automated test, using Python,Redis,stf,docker etc...helping speed up ui testcase execution .
开发分布式系统用来执行自动化测试，用到Python,Redis,stf,docker等，帮助提速提速ui测试用例的执行
* Build ci/cd system including gitlab flow,gitlab pipeline,script tools for kinds of test reports automated collection,analysis and notifications.
为团队开发持续集成和持续交付（ci/cd）系统，包括gitlab flow,gitlab pipeline，各种报告的自动化收集分析通知的脚本工具


### 2017.3 - 2018.6 [Guangzhou Huizhi Communication Co.,Ltd 广州汇智通信技术有限公司](https://www.transocks.com/)
### Senior Android Software Engineer

* **Project** 
* This is an android application like DingTalk offers focused, highly-efficient and secured instant communication solutions, which makes communication easier at work.

* **Responsibilities** 
* Leading design and development of mobile solutions
* Work on specification, development, and debugging of mobile applications
* Facilitating design workshops with business and technical stakeholders to gain buy-in to design proposals

### 2016.8 - 2018.12 [Transocks(没英文名) 成都飞欧比网络科技有限公司](https://www.transocks.com/)
### Senior Android Software Engineer （part-time employees）

* **Project** 
* vpn products running on mobile and tv. https://play.google.com/store/apps/details?id=com.fobwifi.normal

* **Responsibilities** 
* Participate in the construction of the project, choose the reasonable architecture, package by feature appropriately, which promote development efficiency for  team.
参与开发项目，架构选型，按功能划分模块，帮助团队能更好分工合作
* Add core feature:the ui interface,getting vpn lines list via http,google admob,google payment etc.. vpn functions base on [shadowsocks-android open source project](https://github.com/shadowsocks/shadowsocks-android)
vpn功能基于开源项目，基于该我们增加核心特性：用户界面，在线通过Http获取vpn线路，google广告，google pay支付等辅助功能
* Write a static library, providing a private communication encryption protocol with android ndk.
利用android ndk技术，编写了一个静态库，提供私有加密协议功能。


### 2015.7 - 2016.8 (没英文名) 广州小龄童信息科技有限公司
### Senior Android Software Engineer

* **Project** 
* Mobile application enabled parents and teachers communicate online
* https://www.tianyancha.com/brand/b2ee3128075

* **Responsibilities** 

* As Android team leader, leading team to finish the application from the beginning.
作为android团队组长，带领团队从零开始完整实现整个app
* Participate in the construction of the project, choose the reasonable architecture, package by feature appropriately, which promote  development efficiency for  team.
参与开发项目，架构选型，按功能划分模块，帮助团队能更好分工合作

### 2011.9 - 2015.6 [Joyy Inc. 广州华多网络科技有限公司 NASDAQ：YY](https://www.yy.com/)
### C++ Software Engineer, Android Software Engineer 
* (类似岗位：https://app.mokahr.com/apply/hjsd/48#/job/39919c19-076d-4ade-b537-df70eee6e456)

- **Project** platform that provids game information online

* **Responsibilities** 
* As a C/C++ software engineer,developing and maintaining kinds of c++ microservice backend module,using c++,python,mysql,thrift(2011-2012)
* Engaged in the development of android applications that provided game information online(2013-2015)

### 2010.8 - 2011.9 (没英文名) 广州市聚晖电子科技有限公司
### **C/C++ Software Engineer** 

* I was responsible for voip products using Android,Asterisk,sip,Linux,c++...
我负责voip产品研发，基于android系统，Asterisk，sip协议,Linux,c++等

### 2005.7 - 2010.8 Guangzhou Cell Communication Technology Co.,Ltd. 广州华工信元通信技术有限公司
### **C/C++ Software Engineer** 

* I was responsible for application on windows basing on b/s architecture.Providing telecom operators with bill statistics query functions,using powerbuilder,sybase,c++,mfc etc...
我负责windows桌面系统上运行的，基于b/s架构的客户端工具。为运营商提供账单查询统计功能。主要用到powerbuilder,sybase,c++,mfc等技术
