1. Can you show me some examples of your previous work/experience?

- Have you published app on app store before? If yes, please provide a link.
<br/>Latest apps:VPN products run on mobile and tv. base on java and ndk.
<br/>https://play.google.com/store/apps/details?id=com.fobwifi.normal
<br/>https://play.google.com/store/apps/details?id=com.fobwifi.transocks.tv
<br/>Some older apps are out of services,and some apps are to Goverment so not published on app store.

- Any experience on webapp?
<br/> Yes, some projects I experienced  were based on web app. I worked as a native Android developer,exposed native to javascript throught jsbridge, also I can read and write javascript.
<br/> As I know,webapps may have trouble of blank screen because of the heavy webview,there are something we can do:
<br/> 1. As for frontend,less http request,lazy loading,less frontend code(html,css,javascript),Server Side Rendering...may help
<br/> 2. As for android native,we can make use of precreated webview,packaging html resources in app,caching the http relatived resources in memory or local sdcard.

- Provide your preferred set of tools/services to use
<br/> https://github.com/greenrobot/EventBus implifies the communication between components
<br/> https://github.com/greenrobot/greenDAO
<br/> https://github.com/sqlcipher/sqlcipher SQLCipher extends the SQLite database library to add security enhancements that make it more suitable for encrypted local data storage.
<br/> OkHttp & Retrofit
<br/> https://github.com/bumptech/glide
<br/> espresso,monkey test,gitlab pipeline
...
2. What size companies have you worked with in the past?
- Huya Inc. 虎牙信息科技有限公司 NYSE: HUYA 2000+ employees(2018-present)
- [Transocks(vpn) 成都飞欧比网络科技有限公司](https://www.transocks.com/) 8-10 employees(2016.8 - 2018.12)
- Joyy Inc. 广州华多网络科技有限公司 NASDAQ：YY 2000+ employees(2011-2015)

3. Do you have a strong understanding of my business and its audience?
- I am afraid not yet,I would if I had the opportunity.
4. building the features:
- I want the mobile app to allow users to create/view/edit invoices, expenses, payroll, a book record, etc.
<br/>Base on MVC（Model-View-Controller）pattern:
<br/>1. The Model layer communicates with the database and network layers. It is responsible for handing https request when other two layers save and get the data online (in terms of restful api:create,update,get invoices,expenses,etc),and DAO locally,such as the draft management,etc;
<br/>2. The Controller layer acts as bridge between the View and the Model. It contains the core application logic and gets informed of the user’s behavior and updates the Model as needed.
<br/>3. The View layer is the UI(User Interface) layer that holds components that are visible on the screen.It tells the Controller to modify the data, gets data from the Model to refresh the UI,As for android,it should be activity/fragment,layout/xml,etc.
- Activate different features based on subscription tier level
<br/>1. We can manage the user subscription tier level information on the backend.
<br/>2. On the app/frontend, block all the features need subscription tier level until user logined.
<br/>3. When logined,return subscription tier level from backend,then enable features relatived according to the level.
<br/>4. The backend services should also block the features not subscribed by the user.
- push notifications 
- allow users to enable/disable push notifications 
<br/> 1. Allow user enable/disable push notifications on an activity, save the settings in sqlite/sharedpreferences on the phone.
<br/> 2. Also save these settings on the backend(option) 
- Describe how you decide the right time to send push notifications to users (Push notifications can be annoying to users)
<br/> 3. Publish notifications to the users who care(according to subscription tier level and notifications settings saved on the backend)
<br/> 4. Pull the latest important notice when app launched(option,in case of the lost notification publishments)
<br/> 5. On the app,choose the notification style according to its importance，for examples:
<br/> Urgent: Makes a sound and appears as a heads-up notification.
<br/> High: Makes a sound.
<br/> Medium: No sound.
<br/> Low: No sound and does not appear in the status bar.
- weekly reports for users (email)
<br/> backend service is enough.for examples,use cron jobs in linux,jobs can be defined as followed:
<br/> 1. list users need reported(last reported one week ago)
<br/> 2. send emails to those users
<br/> 3. save report time

5. Describe how you decide on the app design and user experience. Give an example.
<br/> 1. In terms of project architecture,I prefer multi-module projects,in which I put independent basic components in different module,and package them to aar files,which I can include in many different apps,so that I can reuse the code, hide implementation details in the aar modules.
<br/> 2. In terms of code design,I will try to use mvc or mvp pattern,as mentioned in 4.1 (create/view/edit invoices).
<br/> 3. For better user experience,we should try our best to avoid anr.we should not run the time consuming code in the main thread. 
<br/> 4. We should make use of local cache to avoid network request unnecessary, for example,cache the icon resources in local files, and cache them in memory when running.
<br/> 5. Lazy loading,and asynchronous loading,especially when enter to the main activity,in order to save user time.
<br/> 6. Should always let users know what is going on,for example, use the loading animation to inform user the app status.

6. What are some specific advantages and disadvantages of both Android and iOS?
- how long it takes from submission to publication of the app
<br/> In Android, any new publication can be done easily and without any review process,and easy access to the Android App Market.Android platform is more friendly in automated testing,because Android is more open.
<br/> While iOS has a review process, when developers want to publish an app they need to send it to Apple for review that takes around several days.
- how to resolve any issues that arise with the publication of the app
<br/> 1. Beta testing before 100% publication of the app is essential,we can gives a nearly finished product to a small group of target users in the real world to do the beta testing,so that we can control the number of user affected by the issues in the publication.
<br/> 2. For Android,beta testing is built-in features in the google store.
<br/> 3. Also we can do beta testing by publishing on the other channel,official website,for example,which is easier than iOS platform.
<br/> 4. We can fix any issues arise during beta testing,and then suggest or force those users affected to upgrade.
<br/> 5. Roll-back is difficult,I think we can disable some features relatived when issues arise, by changing configuration on the backend when necessary.
7. How do you handle security issues?
- we store personal information, invoices, payments, etc
<br/> 1. Use the internal storage but not the external storage.
<br/> 2. Encrypt user data, for example,use SQLCipher(which use 256-bit AES to encrypt the data),or content provider.
<br/> 3. How to store the key is a problem,maybe we can use Android Keystore.
<br/> 4. Or we can store the data on the backend,and use https protocol to communicates.
<br/> 5. Use FingerprintManager to keep others away from the app.
- if there was an attack to the software, how would you resolve it?
<br/> 1. We can not completely prevent people from breaking our app. All we can do is make it slightly more difficult to get in and understand the code.
<br/> 2. Firstly,we should make use of proguard tools when package,or take advantages of android ndk to deter people from reverse engineering our app.
<br/> 3. Secondly,we should have methods to detect the attack.such as verifying app's signing certificate at runtime,or doing some environment checks to identify the emulator.
<br/> 4. Finally,when we detect the attack,we can choose to simply kill the app,or we can pretent to be not knowing of that,but refuse to service it by returning fake message.
8. How can you help me make money off my app?
<br/> 1. in-app advertising,google admob,including banner,interstitial,rewarded video...
<br/> 2. Google Play's billing,including One-time products and subscription. 
9. What information do you need before you start coding an app?
<br/> 1. What is the app:what are the main features,who uses it,is it a demo or a real product...
<br/> 2. Who are the team members.who are the backend developer,the UI designer,the app developer,the tester...
<br/> 3. When is the deadline,when to intergrate with the backend,when to test...
<br/> 4. How will we do it.how to cooperate with the other team members,what architecture will we choose,what components will we use...
10. How will you handle app testing?
<br/> 1. Unittest for the stable and important interface(mock the data from external).
<br/> 3. Static code analysis.(SonarQube,android lint...)
<br/> 2. Self feature test before handing to tester.
<br/> 3. Monkey test to detect crash.
11. How will maintenance of the app work?
<br/> 1.Use crash report tools to prioritize and fix the most pervasive crashes based on the impact on real users
(Firebase Crash Reporting,etc)
<br/> 2.View and analyze the app's ratings and reviews in the app store,and fix the most common issue.
